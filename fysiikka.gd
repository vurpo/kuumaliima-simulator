extends Spatial

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var glue = preload("res://glue.tscn")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_input(true)

func _input(event):
	if (event.is_action_pressed("fire")):
		if (get_node("fps controller/Camera/pistooli").enabled):
			if (get_node("fps controller/Camera/pistooli/spatial/RayCast").is_colliding()):
				var glue_position = get_node("fps controller/Camera/pistooli/spatial/RayCast").get_collision_point()
				var glue_normal = get_node("fps controller/Camera/pistooli/spatial/RayCast").get_collision_normal()
				var glue_inst = glue.instance()
				add_child(glue_inst)
				glue_inst.set_global_transform(Transform(Matrix3(Vector3(0,0,0), 0), glue_position))
				var up = Vector3(0,1,0)
				if (glue_normal.dot(up) > 0.99):
					up = get_node("fps controller/Camera 1").get_global_transform().basis.y
				glue_inst.look_at(glue_position-glue_normal, up)
				glue_inst.set_translation(glue_position)