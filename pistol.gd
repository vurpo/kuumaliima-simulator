extends Spatial

var enabled = false
var rot = Vector2(0,0)

var use_mouse = true

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	set_process_input(true)
	#set_as_toplevel(true)

func _input(event):
	if (enabled):
		if (event.type == InputEvent.MOUSE_MOTION):
			use_mouse = true
			var position = get_node("spatial").get_translation()
			position.x = 0
			position.z = clamp(position.z-(event.relative_x/OS.get_window_size().x), -1, 1)
			position.y = clamp(position.y-(event.relative_y/OS.get_window_size().y), -1, 1)
			get_node("spatial").set_translation(position)
		elif (event.type == InputEvent.JOYSTICK_MOTION):
			use_mouse = false
	else:
		use_mouse = false

func _process(delta):
	if (get_node("spatial/RayCast").is_colliding()):
		var collision_point = get_node("spatial/RayCast").get_collision_point()
		var collision_normal = get_node("spatial/RayCast").get_collision_normal()
		get_node("spatial/target").set_global_transform(Transform(Matrix3(Vector3(0,0,0),0), collision_point+collision_normal*0.01))
		#var up = Vector3(0,1,0)
		var up = Vector3(0,1,0)
		if (collision_normal.dot(Vector3(0,1,0)) > 0.99):
			up = get_global_transform().basis.y
		get_node("spatial/target").look_at(collision_point-collision_normal, up)
		get_node("spatial/target").set_scale(Vector3(0.3,0.3,0.3))
		get_node("spatial/target").show()
	else:
		get_node("spatial/target").hide()
		
	
	var pistol_spatial = get_node("spatial")
	
	var joy_right = Vector2(0,0)
	var joy_left = Vector2(0,0)
	if (enabled):
		joy_right = Vector2(-Input.get_joy_axis(0, JOY_ANALOG_1_X), Input.get_joy_axis(0, JOY_ANALOG_1_Y))
		#var joy = Vector2(int(Input.is_key_pressed(KEY_D))-int(Input.is_key_pressed(KEY_A)), int(Input.is_key_pressed(KEY_W))-int(Input.is_key_pressed(KEY_S)))
		rot += joy_right*delta
		rot.x = clamp(rot.x, -PI/2, PI/2)
		rot.y = clamp(rot.y, -PI/2, PI/2)
		
		joy_left = Vector2(-Input.get_joy_axis(0, JOY_ANALOG_0_X), -Input.get_joy_axis(0, JOY_ANALOG_0_Y))
	
	#set_rotation(Vector3(0, rot.x, rot.y))
	var current_rot = pistol_spatial.get_rotation()
	pistol_spatial.set_rotation(Vector3(0, lerp(current_rot.y, joy_right.x, 0.2), lerp(current_rot.z, joy_right.y, 0.2)))
	
	pistol_spatial.set_scale(Vector3(1,1,1))
	
	if (not use_mouse):
		var current_trans = pistol_spatial.get_translation()
	
		pistol_spatial.set_translation(Vector3(0, lerp(current_trans.y, joy_left.y, 0.1), lerp(current_trans.z, joy_left.x, 0.1)))
	##global_translate(Vector3(0, -Input.get_joy_axis(0, JOY_ANALOG_0_Y), -Input.get_joy_axis(0, JOY_ANALOG_0_X))*10*delta)
