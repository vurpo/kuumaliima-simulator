extends RigidBody

const LOOKING_SPEED = Vector2(PI, PI) # radians/second
const WALKING_SPEED = 2 # meters/second

var rotation = Vector2(0,0)

var glue_gun_focus = Vector3(0,0,0)
var glue_gun_normal = Vector3(0,0,0)
var old_camera_transform

var fps_enabled = true
var use_mouse_keyboard = false

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	set_fixed_process(true)
	set_process_input(true)
	set_process(true)

func _input(event):
	if (event.type == InputEvent.MOUSE_MOTION or event.type == InputEvent.KEY):
		use_mouse_keyboard = true
	elif (event.type == InputEvent.JOYSTICK_MOTION):
		use_mouse_keyboard = false
	
	if (event.type == InputEvent.MOUSE_MOTION):
		if (fps_enabled):
			var look_delta = Vector2(0,0)
			look_delta = Vector2(event.relative_x, event.relative_y)/OS.get_window_size()*-1
			rotation += look_delta*LOOKING_SPEED
			rotation.x = fmod(rotation.x, PI*2)
			rotation.y = clamp(rotation.y, -PI/2, PI/2)
	
	if (event.is_action_pressed("toggle_glue_control")):
		if (fps_enabled):
			if (get_node("Camera/pistooli/spatial/RayCast").is_colliding()):
				glue_gun_focus = get_node("Camera/pistooli/spatial/RayCast").get_collision_point()
				glue_gun_normal = get_node("Camera/pistooli/spatial/RayCast").get_collision_normal()
				old_camera_transform = get_node("Camera 1").get_global_transform()
				var up = Vector3(0,1,0)
				if (glue_gun_normal.dot(up) > 0.99):
					up = old_camera_transform.basis.y
				get_node("Camera 1").look_at_from_pos(glue_gun_focus+glue_gun_normal*1.5, glue_gun_focus, up)
				get_node("Camera").set_interpolation_enabled(true)
				fps_enabled = false
				get_node("Camera/pistooli").enabled = true
		else:
			get_node("Camera 1").set_global_transform(old_camera_transform)
			get_node("Camera").set_interpolation_enabled(false)
			get_node("Camera/pistooli").enabled = false
			fps_enabled = true

func _fixed_process(delta):
	if (fps_enabled):
		var look_delta = Vector2(0,0)
		if (use_mouse_keyboard):
			pass
			#look_delta = Input.get_mouse_speed()/OS.get_window_size()*-1
		else:
			look_delta = Vector2(-Input.get_joy_axis(0, JOY_ANALOG_1_X), -Input.get_joy_axis(0, JOY_ANALOG_1_Y))
		rotation += look_delta*LOOKING_SPEED*delta
		rotation.x = fmod(rotation.x, PI*2)
		rotation.y = clamp(rotation.y, -PI/2, PI/2)
		set_rotation(Vector3(0, rotation.x, 0))
		get_node("Camera 1").set_rotation(Vector3(rotation.y, 0, 0))
		
		var walk_delta = Vector2(0,0)
		if (use_mouse_keyboard):
			walk_delta = Vector2(int(Input.is_action_pressed("strafe right"))-int(Input.is_action_pressed("strafe left")), int(Input.is_action_pressed("backward"))-int(Input.is_action_pressed("forward")))
		else:
			walk_delta = Vector2(Input.get_joy_axis(0, JOY_ANALOG_0_X), Input.get_joy_axis(0, JOY_ANALOG_0_Y))
		translate(Vector3(walk_delta.x, 0, walk_delta.y)*delta*WALKING_SPEED)
		#set_linear_velocity(Vector3(walk_delta.x, get_linear_velocity().y/WALKING_SPEED, walk_delta.y)*WALKING_SPEED)
		
	if (not get_node("Camera").is_interpolation_enabled()):
		get_node("Camera").set_global_transform(get_node("Camera 1").get_global_transform())